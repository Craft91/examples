'use strict';

(function($) {
    jQuery.fn.chrisSearch = function(hints) {
        function init() {

            var
                    $input = $(this),
                    $hintsList,
                    $hints;

            $hintsList = generateList($input).appendTo($input.parent());

            $hints = $hintsList.children();

            $hints
                    // prevent input blur
                    .mousedown(function(e) {
                        e.preventDefault();
                    })

                    .click(function() {
                        $input.val($(this).text()).trigger('input');
                    });

            inputEvents($input, $hintsList);
        }

        /**
         * binds events to input: changing value, pressing up/down arrow & enter
         * 
         * @param {jQuery} $input
         * @param {jQuery} $hintsList list of all hints
         * @returns {undefined}
         */
        function inputEvents($input, $hintsList) {
            $input
                    .on('input', function() {
                        search($input.val(), $hintsList);
                    })

                    .keydown(function(e) {
                        if ([40, 38, 13].indexOf(e.which) === -1) {
                            return;
                        }

                        // choose the highlighted hint and prevent sending form if enter pressed
                        if (e.which === 13) {
                            if ($hintsList.children().filter('.highlighted').click().length) {
                                e.preventDefault();
                            }

                            return;
                        }

                        // prevent moving cursor to the beginning or the end
                        e.preventDefault();

                        onArrow($hintsList, e.which === 40);
                    })

                    .blur(function() {
                        $hintsList.hide();
                    });
        }

        /**
         * allows running through the hints list using arrows
         * 
         * @param {jQuery} $hintsList
         * @param {boolean} down determines which arrow was pressed
         * @returns {undefined}
         */
        function onArrow($hintsList, down) {
            var
                    $sibling,
                    $matching = $hintsList.children().filter(':visible'),
                    $highlighted = $matching.filter('.highlighted');

            // highlight the first/last element if none is highlighted
            if (!$highlighted.length) {
                if (down) {
                    $matching.first().addClass('highlighted');
                } else {
                    $matching.last().addClass('highlighted');
                }

                return;
            }

            $highlighted.removeClass('highlighted');

            $sibling = siblingMatching($highlighted, !down, $matching);

            if ($sibling) {
                $sibling.addClass('highlighted');
                return;
            }

            if (down) {
                $matching.first().addClass('highlighted');
                return;
            }

            $matching.last().addClass('highlighted');
        }

        /**
         * @param {jQuery} $highlighted currently highlighted hint
         * @param {boolean} previous determines whether to look for previous or next hint
         * @param {jQuery} $matching list of matching hints
         * @returns {jQuery|Boolean} previous/next matching hint or false if there isn't one
         */
        function siblingMatching($highlighted, previous, $matching) {
            var
                    found = false,
                    toReturn = false;

            if (previous) {
                $matching = $($matching.get().reverse());
            }

            $matching.each(function() {
                if (found) {
                    toReturn = $(this);
                    return false;
                }

                if (this === $highlighted[0]) {
                    found = true;
                }
            });

            return toReturn;
        }

        /**
         * 
         * @param {jQuery} $input for positioning the list
         * @returns {jQuery} list of hints as a jQuery object
         */
        function generateList($input) {
            var $list = $('<ul/>')
                    .addClass('hints-list')
                    .css({
                        top: $input.outerHeight(),
                        left: 0,
                        width: $input.outerWidth()
                    })
                    .hide();

            $.each(hints, function(index, hint) {
                $('<li/>')
                        .text(hint)
                        .appendTo($list);
            });

            return $list;
        }

        /**
         * searches the list of hints for a specified needle and shows the matching hints
         * 
         * @param {string} needle 
         * @param {jQUery} $hintsList list of all hints
         * @returns {undefined}
         */
        function search(needle, $hintsList) {
            var matchingNumber;

            // make the search case insensitive
            needle = needle.trim().toLocaleLowerCase();

            $hintsList.show().children().hide();

            matchingNumber = 0;

            // don't search if the needle is an empty string
            if (!needle) {
                return;
            }

            $hintsList.children().each(function() {

                // limit the number of hints
                if (matchingNumber > 5) {
                    return false;
                }

                var
                        text = $(this).text().trim(),
                        textLower = text.toLocaleLowerCase(),
                        index = textLower.indexOf(needle);

                if (index === -1) {
                    $(this).removeClass('highlighted');
                    return;
                }

                // bold the mathing string
                $(this)
                        .html(
                                text.substr(0, index)
                                + '<strong>' + text.substr(index, needle.length) + '</strong>'
                                + text.substr(index + needle.length)
                                )
                        .show();

                matchingNumber++;
            });

            // hide hint if only one found and matches exactly the needle 
            if (matchingNumber === 1 && needle.length === $hintsList.children(':visible').text().trim().length) {
                $hintsList.children().hide().removeClass('highlighted');
            }
        }

        return this.each(init);
    };
}(jQuery));
